# PullToRefresh

## 简介

> PullToRefresh是一款OpenHarmony环境下可用的下拉刷新、上拉加载组件。
> 支持设置内置动画的各种属性，支持设置自定义动画，支持lazyForEarch的数据作为数据源。

#### 效果展示：

内置动画效果

![Refresh](gifs/Refresh.gif)![Refresh](gifs/LoadMore.gif)

## 下载安装

```shell
ohpm install @ohos/pulltorefresh
```

## 使用说明

### 快速使用

```typescript
import { RefreshList,RefreshGrid,RefreshScroll,RefreshWaterFlow, PullToRefreshConfigurator } from '@ohos/pulltorefresh'

//动态添加list组件的属性
class MyListModifier implements AttributeModifier<ListAttribute> {
   applyNormalAttribute(instance: ListAttribute): void {
      instance.divider({ strokeWidth: 1, color: 0x222222 });
   }
}     
//RefreshGrid,RefreshScroll,RefreshWaterFlow组件参考如下RefreshList
RefreshList({
// 必传项，列表组件所绑定的数据
data: $data, 
//必传项，控制是否进行下拉刷新
refreshing:$refreshing,
// 必传项，自定义主体布局，内部有列表或宫格组件
customList: () => {
  // 一个用@Builder修饰过的UI方法
  this.getListView();
},
//可选项，属性方法修改时触发
modifier:this.modifier,
// 可选项，下拉刷新回调
onRefresh: () => {
  return new Promise<string>((resolve, reject) => {
    // 模拟网络请求操作，请求网络1秒后得到数据，通知组件，变更列表数据
    setTimeout(() => {
      resolve('刷新成功');
      this.data = this.dataNumbers;
    }, 1000);
  });
},
// 可选项，上拉加载更多回调
onLoadMore: () => {
  return new Promise<string>((resolve, reject) => {
    // 模拟网络请求操作，请求网络1秒后得到数据，通知组件，变更列表数据
    setTimeout(() => {
      resolve('');
      this.data.push("增加的条目" + this.data.length);
    }, 1000);
  });
},
customLoad: null,
customRefresh: null,
})
```
以上系统组件的属性和方法的设置请参考[动态属性设置](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis-arkui/arkui-ts/ts-universal-attributes-attribute-modifier.md)

设置属性示例和设置自定义动画示例请看[示例entry](https://gitee.com/openharmony-sig/PullToRefresh/tree/3.x/entry/src/main/ets/pages)
### 支持lazyForEarch的数据作为数据源
   LazyForEach从提供的数据源中按需迭代数据，并在每次迭代过程中创建相应的组件。当LazyForEach在滚动容器中使用了，框架会根据滚动容器可视区域按需创建组件，当组件滑出可视区域外时，框架会进行组件销毁回收以降低内存占用
   **接口描述：**
```typescript
LazyForEach(
    dataSource: IDataSource,             // 需要进行数据迭代的数据源
    itemGenerator: (item: any, index?: number) => void,  // 子组件生成函数
    keyGenerator?: (item: any, index?: number) => string // 键值生成函数
): void
```
**IDataSource类型说明**
```typescript
interface IDataSource {
    totalCount(): number; // 获得数据总数
    getData(index: number): Object; // 获取索引值对应的数据
    registerDataChangeListener(listener: DataChangeListener): void; // 注册数据改变的监听器
    unregisterDataChangeListener(listener: DataChangeListener): void; // 注销数据改变的监听器
}
```
**DataChangeListener类型说明**
```typescript
interface DataChangeListener {
    onDataReloaded(): void; // 重新加载数据时调用
    onDataAdd(index: number): void; // 添加数据时调用
    onDataMove(from: number, to: number): void; // 数据移动起始位置与数据移动目标位置交换时调用
    onDataDelete(index: number): void; // 删除数据时调用
    onDataChange(index: number): void; // 改变数据时调用
}
```
具体使用请看 openharmony：[LazyForEach：数据懒加载](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/quick-start/arkts-rendering-control-lazyforeach.md/)
## 属性（接口）说明

### PullToRefresh组件属性

|         属性          | 类型                                                             |         释义         | 默认值                         |
|:-------------------:| :-------------------------------------------------------------: |:------------------:| :----------------------------: |
|        data         | Object[]                                                         |   列表或宫格组件所绑定的数据    | undefined                      |
|      modifier       | AttributeModifier<T>                                             |     动态修改组件的属性      | undefined                      |
|     customList      | ```() => void   ```                                             | 自定义主体布局，内部有列表或宫格组件 | undefined                      |
| refreshConfigurator | PullToRefreshConfigurator                                       |       组件属性配置       | PullToRefreshConfigurator      |
|    refreshHeight    | Length                                                          |     触发下拉刷新的偏移量     | undefined              |
|   refreshOpacity    | scale                                                          |    下拉刷新背景图的透明度     | undefined              |
|     loadOpacity     | scale                                                          |    上拉加载背景图的透明度     | undefined              |
|      onRefresh      | ```() => Promise<string>```                                     |       下拉刷新回调       | 1秒后结束下拉刷新动画并提示“刷新失败” |
|     onLoadMore      | ```() => Promise<string>```                                     |      上拉加载更多回调      | 1秒后结束上拉加载动画              |
|    customRefresh    | ```() => void ```                                               |    自定义下拉刷新动画布局     | undefined                      |
|   onAnimPullDown    | ```(value?: number, width?: number, height?: number) => void``` |       下拉中回调        | undefined                      |
|  onAnimRefreshing   | ```(value?: number, width?: number, height?: number) => void``` |       刷新中回调        | undefined                      |
|     customLoad      | ```() => void```                                                |    自定义上拉加载动画布局     | undefined                      |
| onRefreshSpringBack | ```(value?: number,  height?: number) => void``` |       回弹的回调        | undefined                      |
|     refreshing      | boolean |     控制是否进行下拉刷新     | undefined                      |

### PullToRefreshConfigurator类接口

| 接口                       | 参数类型                     |                  释义                  | 默认值            |
| :------------------------:| :-------------------------: |:------------------------------------:| :--------------: |
| setHasRefresh             | boolean                     |              是否具有下拉刷新功能              | true             |
| setHasLoadMore            | boolean                     |              是否具有上拉加载功能              | true             |
| setRefreshHeight          | number                      |                下拉动画高度                | 30               |
| setRefreshColor           | string                      |                下拉动画颜色                | '#999999'        |
| setRefreshBackgroundColor | ResourceColor               |              下拉动画区域背景色               | ''  |
| setRefreshBackgroundImage | ResourceColor               |             下拉刷新动画部分的背景              | 'rgba(0,0,0,0)'  |
| setRefreshTextColor       | ResourceColor               |           下拉加载完毕后提示文本的字体颜色           | '#999999'        |
| setRefreshTextSize        | number 或 string 或 Resource |           下拉加载完毕后提示文本的字体大小           | 18               |
| setRefreshAnimDuration    | number                      |             下拉动画执行一次的时间（自定义动画的回调函数才会触发）              | 600             |
| setRefreshOpacity           | number                      |             下拉刷新背景图的透明度              | 0.5        |
| setLoadImageHeight          | number                      |              上拉动画中图片的高度              | 64               |
| setLoadBackgroundColor    | ResourceColor               |             上拉加载动画部分的背景              |  ''  |
| setLoadBackgroundImage    | ResourceColor               |              上拉动画区域背景色               | 'rgba(0,0,0,0)'   |
| setLoadTextColor          | ResourceColor               |              上拉文本的字体颜色               | '#999999'         |
| setLoadTextSize           | number 或 string 或 Resource |              上拉文本的字体大小               | 18                |
| setLoadOpacity           | number  |              上拉背景图的透明度               | 0.5                |
| setSubjectContentBackgroundColor   | ResourceColor      |               主体内容背景色                | '#eeeeee'   |
| setListSpace        | number                      |           list组件子组件主轴方向的间隔           |  0        |
| setLoadTextLoading        | string                      |             上拉加载更多中时的文本              | '正在玩命加载中...' |
| setFullScreen        | boolean                      |              list組件全屏参数              | false |
| setListInitialIndex        | number                      | list组件设置当前List初次加载时视口起始位置显示的item的索引值 | 0 |
| setLoadAnimDuration        | number                      | 上拉动画执行一次的时间（自定义上拉动画的回调函数才会触发） | 600 |
## 约束与限制

在下述版本验证通过：
- DevEco Studio 5.0 Canary3（5.0.3.300）--SDK:API12

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/PullToRefresh/issues)给我们，当然，我们也非常欢迎你给我们提 [PR](https://gitee.com/openharmony-sig/PullToRefresh/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/PullToRefresh/blob/master/LICENSE) ，请自由地享受和参与开源。
