/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { RefreshScroll, PullToRefreshConfigurator } from '@ohos/pulltorefresh'

//动态添加scroll组件的属性
class MyScrollModifier implements AttributeModifier<ScrollAttribute> {
  applyNormalAttribute(instance: ScrollAttribute): void {
    instance.backgroundColor('#DCDCDC');
    instance.friction(0.6);
    instance.align(Alignment.TopStart);
    instance.nestedScroll({
      scrollForward: NestedScrollMode.PARENT_FIRST,
      scrollBackward: NestedScrollMode.SELF_FIRST
    })
  }
}

@Entry
@Component
struct TabsExample {
  @State fontColor: string = '#182431';
  @State selectedFontColor: string = '#0070FF';
  @State currentIndex: number = 0;
  private controller: TabsController = new TabsController();

  @Builder
  TabBuilder(index: number, name: string) {
    Column() {
      Text(name)
        .fontColor(this.currentIndex === index ? this.selectedFontColor : this.fontColor)
        .fontSize(16)
        .fontWeight(this.currentIndex === index ? 500 : 400)
        .lineHeight(22)
        .margin({ top: 17, bottom: 7 })
      Divider()
        .strokeWidth(2)
        .color('#007DFF')
        .opacity(this.currentIndex === index ? 1 : 0)
    }.width('100%')
  }

  build() {
    Scroll() {
      Column() {
        Text("Scroll Area")
          .width("100%")
          .height("40%")
          .backgroundColor('#0080DC')
          .textAlign(TextAlign.Center)
        Tabs({ barPosition: BarPosition.Start, controller: this.controller }) {
          TabContent() {
            PullToRefreshDemo()
          }.tabBar(this.TabBuilder(0, 'tab1'))

          TabContent() {
            Column().width('100%').height('100%').backgroundColor('#007DFF')
          }.tabBar(this.TabBuilder(1, 'tab2'))
        }
        .height('100%')
        .vertical(false)
        .barMode(BarMode.Fixed)
        .barWidth(360)
        .barHeight(56)
        .animationDuration(400)
        .onChange((index: number) => {
          this.currentIndex = index;
        })
        .width("100%")
      }
    }
    .scrollBar(BarState.Off)
    .width('100%')
    .height('100%')
  }
}

@Component
struct PullToRefreshDemo {
  @State refreshing: boolean = false;
  private dataNumbers: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  private dataStrings: string[] = ['我的评论', '与我相关', '个人中心1', '个人中心2', '个人中心3', '我的发布', '设置', '退出登录'];
  @State data: string[] = this.dataStrings;
  private modifier: MyScrollModifier = new MyScrollModifier();
  private timer: null | number = null;
  @State refreshConfigurator: PullToRefreshConfigurator = new PullToRefreshConfigurator()

  @Styles
  listCard() {
    .backgroundColor(Color.White)
    .height(72)
    .width("100%")
    .borderRadius(12)
  }
  aboutToAppear(): void {
    this.refreshConfigurator.setRefreshBackgroundColor('#DCDCDC')
  }

  build() {
    Column() {
      RefreshScroll({
        // 必传项，列表组件所绑定的数据
        data: $data,
        //必传项，控制是否进行下拉刷新
        refreshing: $refreshing,
        //可选项，组件属性配置，具有默认值
        refreshConfigurator: this.refreshConfigurator,
        // 必传项，需绑定传入主体布局内的列表或宫格组件的item
        customList: () => {
          // 一个用@Builder修饰过的UI方法
          this.getListView();
        },
        //可选项，属性方法修改时触发
        modifier: this.modifier,
        // 可选项，下拉刷新回调
        onRefresh: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络1秒后得到数据，通知组件，变更列表数据
            setTimeout(() => {
              resolve('刷新成功');
              this.data = [...this.dataNumbers];
            }, 1000);
          });
        },
        // 可选项，上拉加载更多回调
        onLoadMore: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络1秒后得到数据，通知组件，变更列表数据
            setTimeout(() => {
              resolve('');
              for (let index = 0; index < 5; index++) {
                this.data.push("增加的条目" + this.data.length);
              }
            }, 1000);
          });
        },
        //可选项，自定义下拉刷新动画布局
        customRefresh: null,
        //可选项，自定义上拉加载动画布局
        customLoad: null,
      })
    }
  }

  @Builder
  private getListView() {
    List({ space: 10 }) {
      ForEach(this.data, (item: string) => {
        ListItem() {
          Text("item" + item)
            .fontSize(16)
        }
        .listCard()
      }, (item: string) => item)
    }
    .width("100%")
    .margin({ top: 10 })
    .edgeEffect(EdgeEffect.Spring)
    .nestedScroll({
      scrollForward: NestedScrollMode.PARENT_FIRST,
      scrollBackward: NestedScrollMode.SELF_FIRST
    })
  }

  aboutToDisAppear() {
    clearTimeout(this.timer);
  }
}