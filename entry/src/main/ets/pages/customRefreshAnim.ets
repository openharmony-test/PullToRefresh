/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { RefreshList } from '@ohos/pulltorefresh'

const pointSpace = 30;
const pointJitterAmplitude = 10;

//动态添加list组件的属性
class MyListModifier implements AttributeModifier<ListAttribute> {
  applyNormalAttribute(instance: ListAttribute): void {
    instance.divider({ strokeWidth: 1, color: 0x222222 });
  }
}

@Entry
@Component
struct Index {
  @State refreshing: boolean = false;
  private dataNumbers: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  private dataStrings: string[] = ['我的评论', '与我相关', '个人中心1', '个人中心2', '个人中心3', '我的发布', '设置', '退出登录'];
  @State data: string[] = this.dataStrings;
  @State modifier: MyListModifier = new MyListModifier();
  //自定义动画
  private canvasSetting: RenderingContextSettings = new RenderingContextSettings(true);
  private canvasRefresh: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.canvasSetting);
  private value1: number[] = [];
  private value2: number[] = [];
  private timer: null | number = null;

  build() {
    Column() {
      RefreshList({
        // 必传项，列表组件所绑定的数据
        data: $data,
        //必传项，控制是否进行下拉刷新
        refreshing: $refreshing,
        // 必传项，需绑定传入主体布局内的列表或宫格组件的item
        customList: () => {
          // 一个用@Builder修饰过的UI方法
          this.getListView();
        },
        //可选项，属性方法修改时触发
        modifier: this.modifier,
        // 可选项，下拉刷新回调
        onRefresh: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络2秒后得到数据，通知组件，变更列表数据
            this.timer = setTimeout(() => {
              resolve('刷新成功');
              this.data = [...this.dataNumbers];
            }, 1000);
          });
        },
        // 可选项，上拉加载更多回调
        onLoadMore: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络1秒后得到数据，通知组件，变更列表数据
            this.timer = setTimeout(() => {
              resolve('');
              for (let index = 0; index < 10; index++) {
                this.data.push("增加的条目" + this.data.length);
              }
            }, 1000);
          });
        },
        // 可选项，自定义下拉刷新动画布局
        customRefresh: () => {
          this.customRefreshView();
        },
        // 可选项，下拉中回调
        onAnimPullDown: (value, width, height) => {
          if (value !== undefined && width !== undefined && height !== undefined) {
            this.canvasRefresh.clearRect(0, 0, width, height);
            if (value <= 0.33) {
              this.drawPoint(width / 2, height / 2);
            } else if (value <= 0.75) {
              this.drawPoint(width / 2 - (pointSpace / 2 * (value - 0.33) / (0.75 - 0.33)), height / 2);
              this.drawPoint(width / 2 + (pointSpace / 2 * (value - 0.33) / (0.75 - 0.33)), height / 2);
            } else {
              this.drawPoint(width / 2, height / 2);
              this.drawPoint(width / 2 - pointSpace / 2 - (pointSpace / 2 * (value - 0.75) / (1 - 0.75)), height / 2);
              this.drawPoint(width / 2 + pointSpace / 2 + (pointSpace / 2 * (value - 0.75) / (1 - 0.75)), height / 2);
            }
          }
        },
        // 可选项，刷新中回调
        onAnimRefreshing: (value, width, height) => {
          if (value !== undefined && width !== undefined && height !== undefined) {
            this.canvasRefresh.clearRect(0, 0, width, height);
            // 将value值由0到1循环变化变为-1到1反复变化
            value = Math.abs(value * 2 - 1) * 2 - 1;
            // 绘制第1个点
            this.drawPoint(width / 2 - pointSpace, height / 2 + pointJitterAmplitude * value);
            // 绘制第2个点
            if (this.value1.length == 7) {
              this.drawPoint(width / 2, height / 2 + pointJitterAmplitude * this.value1[0]);
              this.value1 = this.value1.splice(1, this.value1.length);
            } else {
              this.drawPoint(width / 2, height / 2 + pointJitterAmplitude);
            }
            this.value1.push(value);
            // 绘制第3个点
            if (this.value2.length == 14) {
              this.drawPoint(width / 2 + pointSpace, height / 2 + pointJitterAmplitude * this.value2[0]);
              this.value2 = this.value2.splice(1, this.value2.length);
            } else {
              this.drawPoint(width / 2 + pointSpace, height / 2 + pointJitterAmplitude);
            }
            this.value2.push(value);
          }
        },
        customLoad: null,
      })
    }
  }

  private drawPoint(x: number, y: number): void {
    this.canvasRefresh.beginPath();
    this.canvasRefresh.arc(x, y, 3, 0, Math.PI * 2);
    this.canvasRefresh.fill();
  }

  @Builder
  private customRefreshView() {
    Canvas(this.canvasRefresh)
      .width('100%')
      .height('100%')
      .onReady(() => {
        this.canvasRefresh.fillStyle = 'red';
      })
  }

  @Builder
  private getListView() {
    ForEach(this.data, (item: number) => {
      ListItem() {
        Text('' + item)
          .width('100%')
          .height(150)
          .fontSize(16)
          .textAlign(TextAlign.Center)
          .backgroundColor('#95efd2')
          .borderRadius(10)
      }
    }, (item: string) => item)
  }

  aboutToDisappear() {
    clearTimeout(this.timer);
  }
}