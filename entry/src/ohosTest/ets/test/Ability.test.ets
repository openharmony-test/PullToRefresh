/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { PullToRefreshConfigurator } from '@ohos/pulltorefresh'


export default function abilityTest() {
  describe('ActsAbilityTest', ()=> {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(()=> {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(()=> {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(()=> {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(()=> {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })


    it('TestPullToRefreshConfigurator', 0, ()=> {
      let refreshConfigurator: PullToRefreshConfigurator = new PullToRefreshConfigurator();
      // 设置属性
      refreshConfigurator
        .setRefreshHeight(100)//设置触发刷新的下拉的偏移量
        .setHasRefresh(true)//是否具有下拉刷新功能
        .setHasLoadMore(true)// 是否具有上拉加载功能
        .setListSpace(20)//list组件子组件主轴方向的间隔
        .setRefreshBackgroundImage($r("app.media.sanmartino"))// 下拉刷新动画部分的背景
        .setLoadBackgroundImage($r("app.media.sanmartino"))//// 上拉加载动画部分的背景
        .setListInitialIndex(0)// list组件设置当前List初次加载时视口起始位置显示的item的索引值
        .setRefreshOpacity(0.6)// 下拉刷新背景图的透明度
        .setRefreshTextColor(Color.Black)
        .setRefreshBackgroundColor('#ffbbfaf5')// 下拉动画区域背景色
        .setSubjectContentBackgroundColor(Color.White)// 主体内容背景颜色
        .setLoadBackgroundColor('#ffbbfaf5')//// 上拉动画区域背景色
        .setLoadImageHeight(100)// 上拉图片高度
        .setLoadTextColor(Color.Blue)// 上拉文本的字体颜色
        .setLoadTextSize(30)// 上拉文本的字体大小
        .setLoadTextLoading('玩命加载中...')// 上拉加载更多中时的文本
        .setRefreshAnimDuration(2000)// 下拉动画执行一次的时间
        .setRefreshColor('#ff0000') // 下拉动画颜色设置

      expect(refreshConfigurator.getHasRefresh()).assertTrue();
      expect(refreshConfigurator.getHasLoadMore()).assertTrue();
      expect(refreshConfigurator.getRefreshHeight()).assertEqual(100);
      expect(refreshConfigurator.getListSpace()).assertEqual(20);
      expect(refreshConfigurator.getRefreshBackgroundImage()).assertEqual($r("app.media.sanmartino"));
      expect(refreshConfigurator.getLoadBackgroundImage()).assertEqual($r("app.media.sanmartino"));
      expect(refreshConfigurator.getListInitialIndex()).assertEqual(0);
      expect(refreshConfigurator.getRefreshOpacity()).assertEqual(0.6);
      expect(refreshConfigurator.getRefreshColor()).assertEqual('#ff0000');
      expect(refreshConfigurator.getRefreshBackgroundColor()).assertEqual('#ffbbfaf5');
      expect(refreshConfigurator.getRefreshTextColor()).assertEqual(Color.Black);
      expect(refreshConfigurator.getSubjectContentBackgroundColor()).assertEqual(Color.White);
      expect(refreshConfigurator.getRefreshTextSize()).assertEqual(25);
      expect(refreshConfigurator.getRefreshAnimDuration()).assertEqual(2000);
      expect(refreshConfigurator.getLoadImageHeight()).assertEqual(100);
      expect(refreshConfigurator.getLoadBackgroundColor()).assertEqual('#ffbbfaf5');
      expect(refreshConfigurator.getLoadTextColor()).assertEqual(Color.Blue);
      expect(refreshConfigurator.getLoadTextSize()).assertEqual(30);
      expect(refreshConfigurator.getLoadTextLoading()).assertEqual('玩命加载中...');
    })
    // 目前PullToRefresh等自定义组件内容XTS测试用例无法进行测试
  })
}